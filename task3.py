def appearance(intervals):
    res = 0
    start_lesson, end_lesson = intervals['lesson']
    pup = intervals['pupil']

    pup_starts = [p for i, p in enumerate(pup) if i % 2 == 0]
    pup_ends = [p for i, p in enumerate(pup) if i % 2 == 1]

    tut_starts = [t for i, t in enumerate(intervals['tutor']) if i % 2 == 0]
    tut_ends = [t for i, t in enumerate(intervals['tutor']) if i % 2 == 1]

    for idx, p in enumerate(pup_starts):
        p_end = pup_ends[idx]
        for t_idx, t in enumerate(tut_starts):
            t_end = tut_ends[t_idx]
            max_start = max(p, t)
            min_end = min(p_end, t_end)
            if max_start <= min_end:
                max_start = max(max_start, start_lesson)
                min_end = min(min_end, end_lesson)
                if max_start <= min_end:
                    res += min_end - max_start

    return res


if __name__ == '__main__':
    a = {'lesson': [1594663200, 1594666800],
         'pupil': [1594663340, 1594663389, 1594663390, 1594663395, 1594663396, 1594666472],
         'tutor': [1594663290, 1594663430, 1594663443, 1594666473]}
    b = {'lesson': [1594702800, 1594706400],
         'pupil':
             # [1594702789, 1594704542, 1594704564, 1594706480,
             #       1594706500, 1594706875, 1594706502, 1594706503,
             #       1594706524, 1594706524, 1594706579, 1594706641],
         [1594702789, 1594704500, 1594702807, 1594704542, 1594704512, 1594704513, 1594704564, 1594705150, 1594704581,
          1594704582, 1594704734, 1594705009, 1594705095, 1594705096, 1594705106, 1594706480, 1594705158, 1594705773,
          1594705849, 1594706480, 1594706500, 1594706875, 1594706502, 1594706503, 1594706524, 1594706524, 1594706579,
          1594706641],
         'tutor': [1594700035, 1594700364, 1594702749, 1594705148, 1594705149, 1594706463]}
    c = {'lesson': [1594692000, 1594695600],
         'pupil': [1594692033, 1594696347],
         'tutor': [1594692017, 1594692066, 1594692068, 1594696341]}

    print(appearance(a))
    print(appearance(b))
    print(appearance(c))
