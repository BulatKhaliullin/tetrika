''' В СЛУЧАЕ, КОГДА СНАЧАЛА ИДУТ единицы ПОСЛЕ ИДУТ нули '''
def task(array):
    return ''.join(str(i) for i in array).find('0')


''' В СЛУЧАЕ, КОГДА единицы И нули ИДУТ В ПЕРЕМЕШКУ '''
def task_v2(array):
    index = -1
    for idx, i in enumerate(array):
        if i == 0 and index == -1:
            index = idx
        if i == 1:
            index = -1
    return index


if __name__ == '__main__':
    a = [1, 1, 1, 1, 1, 1, 0, 0, 0, 0]
    print(task(a))
    print(task_v2(a))
