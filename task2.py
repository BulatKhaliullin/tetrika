import requests
from bs4 import BeautifulSoup as bs


wiki_url = 'https://ru.wikipedia.org'
eng = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
rus = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'


def task():
    alphabet = dict.fromkeys(eng + rus, 0)
    next_page_link = '/w/index.php?title=Категория:Животные_по_алфавиту'
    while next_page_link:
        print(wiki_url + next_page_link)
        resp = requests.get(wiki_url + next_page_link)
        soup = bs(resp.text, 'html.parser')
        a = soup.find_all('div', id='mw-pages')

        next_p = a[0].find_all('a', text='Следующая страница')
        if next_p:
            next_page_link = next_p[0].get('href')
        else:
            next_page_link = ''

        names = a[0].find_all('li')
        for i in names:
            print(i.text)
            f_l = i.text[0]
            alphabet[f_l] += 1

    for k, v in alphabet.items():
        print(f'{k}: {v}')


if __name__ == '__main__':
    task()
